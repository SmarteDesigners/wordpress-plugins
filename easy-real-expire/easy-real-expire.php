<?php

/**
 * Plugin Name: Easy Real Expire
 * Plugin URI: http://www.smartedesigners.com/
 * Description: Easy Real Expire
 * Version: 1.0
 * Author: Vinoth Kanth
 * Author URI: http://www.smartedesigners.com
 * Created On : 2021/09/06
 */
//add_action( 'the_content', 'my_thank_you_text' );
//
//function my_thank_you_text ( $content ) {
//    return $content .= '<p>SED Custom plugin is installed successfully!</p>';
//}
// register the meta box for post expiry date
add_action('add_meta_boxes', 'my_custom_field_expire_dates');

function my_custom_field_expire_dates() {
    add_meta_box(
            'my_expirydate_meta_box_id', // this is HTML id of the box on edit screen
            'Property Expire Date', // title of the box
            'my_customfield_expire_date_box_content', // function to be called to display the checkboxes, see the function below
            'property', // on which edit screen the box should appear
            'side', // part of page where the box should appear
            'default'      // priority of the box
    );
}

// display the metabox
function my_customfield_expire_date_box_content() {
    // nonce field for security check, you can have the same
    // nonce field for all your meta boxes of same plugin
    wp_nonce_field(plugin_basename(__FILE__), 'myplugin_nonce');

    // Variables
    global $post; // Get the current post data
    $post_expiry_date = get_post_meta($post->ID, 'my_plugin_expirydate_content', true); // Get the saved values


    if (!empty($post_expiry_date)) {

        echo '<input type="date" name="my_plugin_expirydate_content" value="' . $post_expiry_date . '"  readonly/><br />';
    } else {

        $post_expiry_date = strtotime(DEFAULT_PROPERTY_EXPIRE_DATE);

        echo '<input type="date" name="my_plugin_expirydate_content" value="' . $post_expiry_date . '" readonly/><br />';
    }
}

// save data from checkboxes
add_action('save_post', 'my_custom_expirydate_field_data');

function my_custom_expirydate_field_data($post_id) {

    // check if this isn't an auto save
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
        return;

    // security check
    if (!wp_verify_nonce($_POST['myplugin_nonce'], plugin_basename(__FILE__))) // spelling fix
        return;

    // further checks if you like, 
    // for example particular user, role or maybe post type in case of custom post types
    // now store data in custom fields based on checkboxes selected
    if (isset($_POST['my_plugin_expirydate_content']))
        update_post_meta($post_id, 'my_plugin_expirydate_content', $_POST['my_plugin_expirydate_content']);
    else
        update_post_meta($post_id, 'my_plugin_expirydate_content', 'null');
}

////////////////////////////////////////////
/*         CRON DEMO STARTS HERE           */
/////////////////////////////////////////////
// unschedule event upon plugin deactivation
function cronstarter_deactivate() {
    // find out when the last event was scheduled
    $timestamp = wp_next_scheduled('cronjob_expireproperty');
    // unschedule previous event if any
    wp_unschedule_event($timestamp, 'cronjob_expireproperty');
}

register_deactivation_hook(__FILE__, 'cronstarter_deactivate');

// create a scheduled event (if it does not exist already)
function cronstarter_activation() {
    if (!wp_next_scheduled('cronjob_expireproperty')) {
        wp_schedule_event(time(), 'everyminute', 'cronjob_expireproperty');
    }
}

// and make sure it's called whenever WordPress loads
add_action('wp', 'cronstarter_activation');

// here's the function we'd like to call with our cron job
function after_expire_repeat() {

    // do here what needs to be done automatically as per your schedule
    // in this example we're sending an email
    // components for our email
//        Database post actions
    global $wpdb;

    $db_prefix = $wpdb->prefix;

    $query = "select p.id, pm.meta_value as expire_date, u.user_email, p.post_title"
            . " from $db_prefix posts p"
            . " LEFT JOIN $db_prefix postmeta pm ON pm.post_id = p.id "
            . " LEFT JOIN $db_prefix u ON u.id = p.post_author "
            . " where p.post_type = 'property' and p.post_status = 'publish' and pm.meta_key = 'my_plugin_expirydate_content' ";

    $post_details = $wpdb->get_results($query);

    foreach ($post_details as $post) {

        if (!empty($post->expire_date)) {

            if (strtotime($post->expire_date) < strtotime(date('Y-m-d'))) {

//                Set the post type to draft after post expired.
                $wpdb->update($db_prefix . 'posts', array('post_type' => 'draft'), array('post_id' => $post->id));

//                Sending email to agent(property)
                $recepients = $post->user_email;
                $subject = 'Your property has been expired: ' . $post->post_title;
                $message = 'Your property has been expired.';

                $success = mail($recepients, $subject, $message);

                if ($success) {
                    
                } else {
                    error_log("Error while sending property expiration email to agent.");
                }

//                 Sending email to propcare administration
//                Procare Admins email has assigned in wp-config file.
                $recepients = $post->user_email;
                $subject = 'property expired: ' . $post->post_title;
                $message = 'property expired.';

                $success = mail($recepients, $subject, $message);

                if ($success) {
                    
                } else {
                    error_log("Error while sending property expiration email to propcare admin.");
                }
            }
        }
    }
}

// hook that function onto our scheduled event:
add_action('cronjob_expireproperty', 'after_expire_repeat');

// CUSTOM INTERVALS
// by default we only have hourly, twicedaily and daily as intervals 
// to add your own, use something like this - the example adds 'weekly'
// http://codex.wordpress.org/Function_Reference/wp_get_schedules

function cron_add_weekly($schedules) {
    // Adds once weekly to the existing schedules.
    $schedules['weekly'] = array(
        'interval' => 604800,
        'display' => __('Once Weekly')
    );
    return $schedules;
}

add_filter('cron_schedules', 'cron_add_minute');

// add another interval
function cron_add_minute($schedules) {
    // Adds once every minute to the existing schedules.
    $schedules['everyminute'] = array(
        'interval' => 60,
        'display' => __('Once Every Minute')
    );
    return $schedules;
}

add_filter('cron_schedules', 'cron_add_minute');

//Cron Schedule Test